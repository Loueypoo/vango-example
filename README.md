# vango-example-bitbucket

`Hello, World!` example for [vercel-vango](https://vercel-vango.vercel.app/).

Get example:

```bash
go get vercel-vango.vercel.app/vango-example-bitbucket
```

Testing:

```bash
go test -v vercel-vango.vercel.app/vango-example-bitbucket
```

Build binary:

```bash
go build vercel-vango.vercel.app/vango-example-bitbucket
```

Install binary:

```bash
go install vercel-vango.vercel.app/vango-example-bitbucket
```

Run the installed binary:

```bash
vango-example-bitbucket
```
